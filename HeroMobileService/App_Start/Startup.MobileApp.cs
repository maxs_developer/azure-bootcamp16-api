﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Web.Http;
using HeroMobileService.DataObjects;
using HeroMobileService.Models;
using Microsoft.Azure.Mobile.Server;
using Microsoft.Azure.Mobile.Server.Authentication;
using Microsoft.Azure.Mobile.Server.Config;
using Owin;

namespace HeroMobileService
{
    public partial class Startup
    {
        public static void ConfigureMobileApp(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();

            //For more information on Web API tracing, see http://go.microsoft.com/fwlink/?LinkId=620686 
            config.EnableSystemDiagnosticsTracing();

            config.MapHttpAttributeRoutes(); //(added !!!)

            new MobileAppConfiguration()
                .UseDefaultConfiguration()
                .ApplyTo(config);

            // Use Entity Framework Code First to create database tables based on your DbContext
            Database.SetInitializer(new HeroMobileDbContextInitializer());

            // To prevent Entity Framework from modifying your database schema, use a null database initializer
            // Database.SetInitializer<HeroMobileDbContext>(null);

            MobileAppSettingsDictionary settings = config.GetMobileAppSettingsProvider().GetMobileAppSettings();

            if (string.IsNullOrEmpty(settings.HostName))
            {
                // This middleware is intended to be used locally for debugging. By default, HostName will
                // only have a value when running in an App Service application.
                app.UseAppServiceAuthentication(new AppServiceAuthenticationOptions
                {
                    SigningKey = ConfigurationManager.AppSettings["SigningKey"],
                    ValidAudiences = new[] { ConfigurationManager.AppSettings["ValidAudience"] },
                    ValidIssuers = new[] { ConfigurationManager.AppSettings["ValidIssuer"] },
                    TokenHandler = config.GetAppServiceTokenHandler()
                });
            }
            app.UseWebApi(config);
        }
    }

    public class HeroMobileDbContextInitializer : CreateDatabaseIfNotExists<HeroMobileDbContext>
    {
        protected override void Seed(HeroMobileDbContext context)
        {
            context.Set<Person>().Add(new Person { Id = Guid.NewGuid().ToString(), Name = "Dmytro Mykhailov", Gender = Gender.Man });
            context.Set<Person>().Add(new Person { Id = Guid.NewGuid().ToString(), Name = "Maksym Savchenko", Gender = Gender.Man });
            context.Set<Person>().Add(new Person { Id = Guid.NewGuid().ToString(), Name = "Sergii Kryshtop", Gender = Gender.Man });
            context.Set<Person>().Add(new Person { Id = Guid.NewGuid().ToString(), Name = "Pavel Revenkov", Gender = Gender.Man });
            context.Set<Person>().Add(new Person { Id = Guid.NewGuid().ToString(), Name = "Oleksandr Feschenko", Gender = Gender.Man });

            base.Seed(context);
        }
    }

}

