﻿using Microsoft.Azure.Mobile.Server;

namespace HeroMobileService.DataObjects
{
    public class Person : EntityData
    {
        public string Name { get; set; }
        public string Picture { get; set; }
        public Gender Gender { get; set; }
    }

    public enum Gender
    {
        Man,
        Woman
    }
}