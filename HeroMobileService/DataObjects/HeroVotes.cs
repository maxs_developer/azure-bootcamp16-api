﻿namespace HeroMobileService.DataObjects
{
    public class HeroVotes
    {
        public HeroVotes()
        {
        }

        public HeroVotes(Hero hero, int votesCount)
        {
            Person = hero.Person;
            VotesCount = votesCount;
        }

        public Person Person { get; set; }
        public int VotesCount { get; set; }
    }
}