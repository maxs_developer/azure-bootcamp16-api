﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.Azure.Mobile.Server;

namespace HeroMobileService.DataObjects
{
    public class Hero : EntityData
    {
        public Hero()
        {
            PromotionDate = DateTime.Now;
        }

        //public Hero(string id, string comment)
        //{
        //    PersonId = id;
        //    Comment = comment;
        //}

        public Hero(Person person, string comment)
        {
            Person = person;
            PersonId = person.Id;
            Comment = comment;
        }

        public string PersonId { get; set; }
        [Required]
        public string Comment { get; set; }
        public DateTime PromotionDate { get; set; }

        [ForeignKey("PersonId")]
        public virtual Person Person { get; set; }
    }
}