﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using HeroMobileService.DataObjects;
using HeroMobileService.Models;
using Microsoft.Azure.Mobile.Server.Config;

namespace HeroMobileService.Controllers
{
    [MobileAppController]
    public class SummaryController : ApiController
    {
        [HttpGet]
        [Route("api/latestheroes")]
        public IEnumerable<Hero> LatestHeroes()
        {
            HeroMobileDbContext context = new HeroMobileDbContext();
            return context.Heroes
                .Include(t => t.Person)
                .OrderByDescending(h => h.PromotionDate)
                .Take(10)
                .ToList();
        }

        [HttpGet]
        [Route("api/topheroes")]
        public IEnumerable<HeroVotes> TopHeroes()
        {
            HeroMobileDbContext context = new HeroMobileDbContext();

            return context.Heroes
                .GroupBy(h => h.Person.Id)
                .Select(hg => new HeroVotes { Person = hg.FirstOrDefault().Person, VotesCount = hg.Count() })
                .OrderByDescending(hv => hv.VotesCount)
                .Take(10);
        }
    }
}
