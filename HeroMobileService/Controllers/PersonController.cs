﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using System.Web.Http.Tracing;
using Microsoft.Azure.Mobile.Server;
using HeroMobileService.DataObjects;
using HeroMobileService.Models;

namespace HeroMobileService.Controllers
{
    public class PersonController : TableController<Person>
    {
        private ITraceWriter _traceWriter;

        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);

            _traceWriter = Configuration.Services.GetTraceWriter();

            HeroMobileDbContext context = new HeroMobileDbContext();
            DomainManager = new EntityDomainManager<Person>(context, Request); //MappedEntityDomainManages
        }

        // GET tables/Person
        public IQueryable<Person> GetAllPerson()
        {
            _traceWriter.Info("Get Person table call");
            return Query(); 
        }

        // GET tables/Person/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Person> GetPerson(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Person/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task<Person> PatchPerson(string id, Delta<Person> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Person
        public async Task<IHttpActionResult> PostPerson(Person item)
        {
            Person current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Person/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public Task DeletePerson(string id)
        {
             return DeleteAsync(id);
        }
    }
}
