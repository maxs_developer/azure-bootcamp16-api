﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.OData;
using Microsoft.Azure.Mobile.Server;
using HeroMobileService.DataObjects;
using HeroMobileService.Models;
using Microsoft.Azure.NotificationHubs.Messaging;

namespace HeroMobileService.Controllers
{
    public class HeroController : TableController<Hero>
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
            HeroMobileDbContext context = new HeroMobileDbContext();
            DomainManager = new EntityDomainManager<Hero>(context, Request);
        }

        // GET tables/Hero
        public IQueryable<Hero> GetAllHero()
        {
            return Query(); 
        }

        // GET tables/Hero/48D68C86-6EA6-4C25-AA33-223FC9A27959
        public SingleResult<Hero> GetHero(string id)
        {
            return Lookup(id);
        }

        // PATCH tables/Hero/48D68C86-6EA6-4C25-AA33-223FC9A27959
        [Authorize]
        public Task<Hero> PatchHero(string id, Delta<Hero> patch)
        {
             return UpdateAsync(id, patch);
        }

        // POST tables/Hero
        //[Authorize]
        public async Task<IHttpActionResult> PostHero(Hero item)
        {
            Hero current = await InsertAsync(item);
            return CreatedAtRoute("Tables", new { id = current.Id }, current);
        }

        // DELETE tables/Hero/48D68C86-6EA6-4C25-AA33-223FC9A27959
        //[Authorize]
        public Task DeleteHero(string id)
        {
             return DeleteAsync(id);
        }
    }
}
